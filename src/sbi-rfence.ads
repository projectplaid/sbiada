package SBI.RFENCE is
   procedure Remote_Fence_I
     (Hart_Mask :     UWord; Hart_Mask_Base : UWord; Result : out UWord;
      Error     : out IWord);
   procedure Remote_SFence_VMA
     (Hart_Mask :     UWord; Hart_Mask_Base : UWord; Start_Addr : UWord;
      Result    : out UWord; Error : out IWord);
   procedure Remote_SFence_VMA_ASID
     (Hart_Mask : UWord; Hart_Mask_Base : UWord; Start_Addr : UWord;
      Size      : UWord; ASID : UWord; Result : out UWord; Error : out IWord);
   procedure Remote_HFence_GVMA_VMID
     (Hart_Mask : UWord; Hart_Mask_Base : UWord; Start_Addr : UWord;
      Size      : UWord; VMID : UWord; Result : out UWord; Error : out IWord);
   procedure Remote_HFence_GVMA
     (Hart_Mask : UWord; Hart_Mask_Base : UWord; Start_Addr : UWord;
      Size      : UWord; Result : out UWord; Error : out IWord);
   procedure Remote_HFence_VMA_ASID
     (Hart_Mask : UWord; Hart_Mask_Base : UWord; Start_Addr : UWord;
      Size      : UWord; ASID : UWord; Result : out UWord; Error : out IWord);
   procedure Remote_HFence_VMA
     (Hart_Mask :     UWord; Hart_Mask_Base : UWord; Start_Addr : UWord;
      Result    : out UWord; Error : out IWord);

private
   Extension_Id : constant UWord := 16#5246_4E43#;
end SBI.RFENCE;
