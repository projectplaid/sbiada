package body SBI.Base is
   procedure Get_Spec_Version
     (Major : out UWord; Minor : out UWord; Error : out IWord)
   is
      Value : UWord;
   begin
      Ecall (Base_Extension_Id, 0, Error, Value);

      Major := Shift_Right (Value, 24) and 16#7f#;
      Minor := Value and 16#ff_ffff#;
   end Get_Spec_Version;

   procedure Get_Implementation_Id
     (Implementation_Id   : out UWord;
      Implementation_Type : out SBI_Implementations; Error : out IWord)
   is
   begin
      Ecall (Base_Extension_Id, 1, Error, Implementation_Id);

      case Implementation_Id is
         when 0 =>
            Implementation_Type := Berkeley_Boot_Loader;
         when 1 =>
            Implementation_Type := Open_SBI;
         when 2 =>
            Implementation_Type := Xvisor;
         when 3 =>
            Implementation_Type := KVM;
         when 4 =>
            Implementation_Type := Rust_SBI;
         when 5 =>
            Implementation_Type := Diosix;
         when 6 =>
            Implementation_Type := Coffer;
         when others =>
            Implementation_Type := Other;
      end case;
   end Get_Implementation_Id;

   procedure Get_Implementation_Version
     (Version : out UWord; Error : out IWord)
   is
   begin
      Ecall (Base_Extension_Id, 2, Error, Version);
   end Get_Implementation_Version;

   procedure Probe_Extension
     (Extension_Id : UWord; Result : out UWord; Error : out IWord)
   is
   begin
      Ecall (Base_Extension_Id, 3, Extension_Id, Error, Result);
   end Probe_Extension;

   procedure Get_Machine_Vendor_Id (Result : out UWord; Error : out IWord) is
   begin
      Ecall (Base_Extension_Id, 4, Error, Result);
   end Get_Machine_Vendor_Id;

   procedure Get_Machine_Architecture_Id
     (Result : out UWord; Error : out IWord)
   is
   begin
      Ecall (Base_Extension_Id, 5, Error, Result);
   end Get_Machine_Architecture_Id;

   procedure Get_Machine_Implementation_Id
     (Result : out UWord; Error : out IWord)
   is
   begin
      Ecall (Base_Extension_Id, 6, Error, Result);
   end Get_Machine_Implementation_Id;

end SBI.Base;
