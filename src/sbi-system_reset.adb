package body SBI.System_Reset is

   ------------------
   -- System_Reset --
   ------------------

   procedure System_Reset
     (Reset_Type :     UInt32; Reset_Reason : UInt32; Result : out UWord;
      Error      : out IWord)
   is
   begin
      Ecall
        (Extension_Id, 16#00#, UWord (Reset_Type), UWord (Reset_Reason), Error,
         Result);
   end System_Reset;

end SBI.System_Reset;
