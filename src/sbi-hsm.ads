package SBI.HSM is

   procedure HART_Start
     (Hart_Id : UWord; Start_Addr : UWord; Opaque : UWord; Result : out UWord;
      Error   : out IWord);
   procedure HART_Stop
     (Hart_Id : UWord; Result : out UWord; Error : out IWord);
   procedure HART_Get_Status
     (Hart_Id : UWord; Result : out UWord; Error : out IWord);
   procedure HART_Suspend
     (Suspend_Type :     UInt32; Resume_Addr : UWord; Opaque : UWord;
      Result       : out UWord; Error : out IWord);

private
   Extension_Id : constant UWord := 16#48_534D#;
end SBI.HSM;
