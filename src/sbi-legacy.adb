with System.Machine_Code; use System.Machine_Code;

package body SBI.Legacy is

   procedure Set_Timer
     (Time_Value : UWord; Result : out IWord; Error : out IWord)
   is
   begin
      Ecall (Timer_Extension_Id, 0, Time_Value, Error, Result);
   end Set_Timer;

   procedure Console_Putchar
     (Char : Character; Result : out IWord; Error : out IWord)
   is
   begin
      Ecall
        (Console_Putchar_Extension_Id, 0, Character'Pos (Char), Error, Result);
   end Console_Putchar;

   procedure Console_Getchar (Result : out IWord; Error : out IWord) is
   begin
      Ecall (Console_Getchar_Extension_Id, 0, Error, Result);
   end Console_Getchar;

   procedure Clear_IPI (Result : out IWord; Error : out IWord) is
   begin
      Ecall (Clear_IPI_Extension_Id, 0, Error, Result);
   end Clear_IPI;

   procedure Send_IPI
     (Hart_Mask : Hart_Mask_Type; Result : out IWord; Error : out IWord)
   is
   begin
      Ecall
        (Send_IPI_Extension_Id, 0, Hart_Mask (Hart_Mask'First)'Address, Error,
         Result);
   end Send_IPI;

   procedure Remote_Fence_I
     (Hart_Mask : Hart_Mask_Type; Result : out IWord; Error : out IWord)
   is
   begin
      Ecall
        (Remote_Fence_Extension_Id, 0, Hart_Mask (Hart_Mask'First)'Address,
         Error, Result);
   end Remote_Fence_I;

   procedure Remote_SFence_VMA
     (Hart_Mask :     Hart_Mask_Type; Start : UWord; Size : UWord;
      Result    : out IWord; Error : out IWord)
   is
   begin
      Ecall
        (Remote_SFence_Extension_Id, 0, Hart_Mask (Hart_Mask'First)'Address,
         Start, Size, Error, Result);
   end Remote_SFence_VMA;

   procedure Remote_SFence_VMA_ASID
     (Hart_Mask : Hart_Mask_Type; Start : UWord; Size : UWord; ASID : UWord;
      Result    : out IWord; Error : out IWord)
   is
   begin
      Ecall
        (Remote_SFence_ASID_Extension_Id, 0,
         Hart_Mask (Hart_Mask'First)'Address, Start, Size, ASID, Error,
         Result);
   end Remote_SFence_VMA_ASID;

   procedure Shutdown is
      Error  : IWord;
      Result : UWord;
   begin
      Ecall (System_Shutdown_Extension_Id, 0, Error, Result);
   end Shutdown;

   --  Yes, these are copied from sbi.adb, because for these legacy
   --  calls, they return signed integers

   procedure Ecall
     (Extension_Id :     UWord; Function_Id : UWord; Error : out IWord;
      Value        : out IWord)
   is
      use ASCII;
   begin
      Asm
        ("mv a6, %2" & LF & HT & "mv a7, %3" & LF & HT & "ecall" & LF & HT &
         "mv %0, a0" & LF & HT & "mv %1, a1",
         Inputs  =>
           (UWord'Asm_Input ("g", Function_Id),
            UWord'Asm_Input ("g", Extension_Id)),
         Outputs =>
           (IWord'Asm_Output ("=g", Error), IWord'Asm_Output ("=g", Value)),
         Clobber => "a0,a1", Volatile => True);
   end Ecall;

   procedure Ecall
     (Extension_Id :     UWord; Function_Id : UWord; Arg_0 : UWord;
      Error        : out IWord; Value : out IWord)
   is
      use ASCII;
   begin
      Asm
        ("mv a0, %2" & LF & HT & "mv a6, %3" & LF & HT & "mv a7, %4" & LF &
         HT & "ecall" & LF & HT & "mv %0, a0" & LF & HT & "mv %1, a1",
         Inputs  =>
           (UWord'Asm_Input ("r", Arg_0), UWord'Asm_Input ("r", Function_Id),
            UWord'Asm_Input ("r", Extension_Id)),
         Outputs =>
           (IWord'Asm_Output ("=r", Error), IWord'Asm_Output ("=r", Value)),
         Clobber => "a0,a1", Volatile => True);
   end Ecall;

   procedure Ecall
     (Extension_Id :     UWord; Function_Id : UWord; Arg_0 : System.Address;
      Error        : out IWord; Value : out IWord)
   is
      use ASCII;
   begin
      Asm
        ("mv a0, %2" & LF & HT & "mv a6, %3" & LF & HT & "mv a7, %4" & LF &
         HT & "ecall" & LF & HT & "mv %0, a0" & LF & HT & "mv %1, a1",
         Inputs  =>
           (System.Address'Asm_Input ("r", Arg_0),
            UWord'Asm_Input ("r", Function_Id),
            UWord'Asm_Input ("r", Extension_Id)),
         Outputs =>
           (IWord'Asm_Output ("=r", Error), IWord'Asm_Output ("=r", Value)),
         Clobber => "a0,a1", Volatile => True);
   end Ecall;

   procedure Ecall
     (Extension_Id : UWord; Function_Id : UWord; Arg_0 : System.Address;
      Arg_1 : UWord; Arg_2 : UWord; Error : out IWord; Value : out IWord)
   is
      use ASCII;
   begin
      Asm
        ("mv a0, %2" & LF & HT & "mv a1, %3" & LF & HT & "mv a2, %4" & LF &
         HT & "mv a6, %5" & LF & HT & "mv a7, %6" & LF & HT & "ecall" & LF &
         HT & "mv %0, a0" & LF & HT & "mv %1, a1",
         Inputs  =>
           (System.Address'Asm_Input ("r", Arg_0),
            UWord'Asm_Input ("r", Arg_1), UWord'Asm_Input ("r", Arg_2),
            UWord'Asm_Input ("r", Function_Id),
            UWord'Asm_Input ("r", Extension_Id)),
         Outputs =>
           (IWord'Asm_Output ("=r", Error), IWord'Asm_Output ("=r", Value)),
         Clobber => "a0,a1", Volatile => True);
   end Ecall;

   procedure Ecall
     (Extension_Id :     UWord; Function_Id : UWord; Arg_0 : System.Address;
      Arg_1        : UWord; Arg_2 : UWord; Arg_3 : UWord; Error : out IWord;
      Value        : out IWord)
   is
      use ASCII;
   begin
      Asm
        ("mv a0, %2" & LF & HT & "mv a1, %3" & LF & HT & "mv a2, %4" & LF &
         HT & "mv a3, %5" & LF & HT & "mv a6, %6" & LF & HT & "mv a7, %7" &
         LF & HT & "ecall" & LF & HT & "mv %0, a0" & LF & HT & "mv %1, a1",
         Inputs  =>
           (System.Address'Asm_Input ("r", Arg_0),
            UWord'Asm_Input ("r", Arg_1), UWord'Asm_Input ("r", Arg_2),
            UWord'Asm_Input ("r", Arg_3), UWord'Asm_Input ("r", Function_Id),
            UWord'Asm_Input ("r", Extension_Id)),
         Outputs =>
           (IWord'Asm_Output ("=r", Error), IWord'Asm_Output ("=r", Value)),
         Clobber => "a0,a1", Volatile => True);
   end Ecall;

end SBI.Legacy;
