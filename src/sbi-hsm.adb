package body SBI.HSM is

   ----------------
   -- HART_Start --
   ----------------

   procedure HART_Start
     (Hart_Id : UWord; Start_Addr : UWord; Opaque : UWord; Result : out UWord;
      Error   : out IWord)
   is
   begin
      Ecall (Extension_Id, 16#00#, Hart_Id, Start_Addr, Opaque, Error, Result);
   end HART_Start;

   ---------------
   -- HART_Stop --
   ---------------

   procedure HART_Stop (Hart_Id : UWord; Result : out UWord; Error : out IWord)
   is
   begin
      Ecall (Extension_Id, 16#01#, Hart_Id, Error, Result);
   end HART_Stop;

   ---------------------
   -- HART_Get_Status --
   ---------------------

   procedure HART_Get_Status
     (Hart_Id : UWord; Result : out UWord; Error : out IWord)
   is
   begin
      Ecall (Extension_Id, 16#02#, Hart_Id, Error, Result);
   end HART_Get_Status;

   ------------------
   -- HART_Suspend --
   ------------------

   procedure HART_Suspend
     (Suspend_Type :     UInt32; Resume_Addr : UWord; Opaque : UWord;
      Result       : out UWord; Error : out IWord)
   is
   begin
      Ecall
        (Extension_Id, 16#03#, UWord (Suspend_Type), Resume_Addr, Opaque,
         Error, Result);
   end HART_Suspend;

end SBI.HSM;
