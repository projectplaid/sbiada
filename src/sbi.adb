with System.Machine_Code; use System.Machine_Code;

package body SBI is

   procedure Ecall
     (Extension_Id :     UWord; Function_Id : UWord; Error : out IWord;
      Value        : out UWord)
   is
      use ASCII;
   begin
      Asm
        ("mv a6, %2" & LF & HT & "mv a7, %3" & LF & HT & "ecall" & LF & HT &
         "mv %0, a0" & LF & HT & "mv %1, a1",
         Inputs  =>
           (UWord'Asm_Input ("g", Function_Id),
            UWord'Asm_Input ("g", Extension_Id)),
         Outputs =>
           (IWord'Asm_Output ("=g", Error), UWord'Asm_Output ("=g", Value)),
         Clobber => "a0,a1", Volatile => True);
   end Ecall;

   procedure Ecall
     (Extension_Id :     UWord; Function_Id : UWord; Arg_0 : UWord;
      Error        : out IWord; Value : out UWord)
   is
      use ASCII;
   begin
      Asm
        ("mv a0, %2" & LF & HT & "mv a6, %3" & LF & HT & "mv a7, %4" & LF &
         HT & "ecall" & LF & HT & "mv %0, a0" & LF & HT & "mv %1, a1",
         Inputs  =>
           (UWord'Asm_Input ("r", Arg_0), UWord'Asm_Input ("r", Function_Id),
            UWord'Asm_Input ("r", Extension_Id)),
         Outputs =>
           (IWord'Asm_Output ("=r", Error), UWord'Asm_Output ("=r", Value)),
         Clobber => "a0,a1", Volatile => True);
   end Ecall;

   procedure Ecall
     (Extension_Id : UWord; Function_Id : UWord; Arg_0 : UWord; Arg_1 : UWord;
      Error        : out IWord; Value : out UWord)
   is
      use ASCII;
   begin
      Asm
        ("mv a0, %2" & LF & HT & "mv a1, %3" & LF & HT & "mv a6, %4" & LF &
         HT & "mv a7, %5" & LF & HT & "ecall" & LF & HT & "mv %0, a0" & LF &
         HT & "mv %1, a1",
         Inputs  =>
           (UWord'Asm_Input ("r", Arg_0), UWord'Asm_Input ("r", Arg_1),
            UWord'Asm_Input ("r", Function_Id),
            UWord'Asm_Input ("r", Extension_Id)),
         Outputs =>
           (IWord'Asm_Output ("=r", Error), UWord'Asm_Output ("=r", Value)),
         Clobber => "a0,a1", Volatile => True);
   end Ecall;

   procedure Ecall
     (Extension_Id : UWord; Function_Id : UWord; Arg_0 : UWord; Arg_1 : UWord;
      Arg_2        : UWord; Error : out IWord; Value : out UWord)
   is
      use ASCII;
   begin
      Asm
        ("mv a0, %2" & LF & HT & "mv a1, %3" & LF & HT & "mv a2, %4" & LF &
         HT & "mv a6, %5" & LF & HT & "mv a7, %6" & LF & HT & "ecall" & LF &
         HT & "mv %0, a0" & LF & HT & "mv %1, a1",
         Inputs  =>
           (UWord'Asm_Input ("r", Arg_0), UWord'Asm_Input ("r", Arg_1),
            UWord'Asm_Input ("r", Arg_2), UWord'Asm_Input ("r", Function_Id),
            UWord'Asm_Input ("r", Extension_Id)),
         Outputs =>
           (IWord'Asm_Output ("=r", Error), UWord'Asm_Output ("=r", Value)),
         Clobber => "a0,a1,a2", Volatile => True);
   end Ecall;

   procedure Ecall
     (Extension_Id : UWord; Function_Id : UWord; Arg_0 : UWord; Arg_1 : UWord;
      Arg_2 : UWord; Arg_3 : UWord; Error : out IWord; Value : out UWord)
   is
      use ASCII;
   begin
      Asm
        ("mv a0, %2" & LF & HT & "mv a1, %3" & LF & HT & "mv a2, %4" & LF &
         HT & "mv a3, %5" & LF & HT & "mv a6, %6" & LF & HT & "mv a7, %7" &
         LF & HT & "ecall" & LF & HT & "mv %0, a0" & LF & HT & "mv %1, a1",
         Inputs  =>
           (UWord'Asm_Input ("r", Arg_0), UWord'Asm_Input ("r", Arg_1),
            UWord'Asm_Input ("r", Arg_2), UWord'Asm_Input ("r", Arg_3),
            UWord'Asm_Input ("r", Function_Id),
            UWord'Asm_Input ("r", Extension_Id)),
         Outputs =>
           (IWord'Asm_Output ("=r", Error), UWord'Asm_Output ("=r", Value)),
         Clobber => "a0,a1,a2,a3", Volatile => True);
   end Ecall;

   procedure Ecall
     (Extension_Id : UWord; Function_Id : UWord; Arg_0 : UWord; Arg_1 : UWord;
      Arg_2        : UWord; Arg_3 : UWord; Arg_4 : UWord; Error : out IWord;
      Value        : out UWord)
   is
      use ASCII;
   begin
      Asm
        ("mv a0, %2" & LF & HT & "mv a1, %3" & LF & HT & "mv a2, %4" & LF &
         HT & "mv a3, %5" & LF & HT & "mv a4, %6" & LF & HT & "mv a6, %7" &
         LF & HT & "mv a7, %8" & LF & HT & "ecall" & LF & HT & "mv %0, a0" &
         LF & HT & "mv %1, a1",
         Inputs  =>
           (UWord'Asm_Input ("r", Arg_0), UWord'Asm_Input ("r", Arg_1),
            UWord'Asm_Input ("r", Arg_2), UWord'Asm_Input ("r", Arg_3),
            UWord'Asm_Input ("r", Arg_4), UWord'Asm_Input ("r", Function_Id),
            UWord'Asm_Input ("r", Extension_Id)),
         Outputs =>
           (IWord'Asm_Output ("=r", Error), UWord'Asm_Output ("=r", Value)),
         Clobber => "a0,a1,a2,a3,a4", Volatile => True);
   end Ecall;

end SBI;
