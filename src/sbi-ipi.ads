package SBI.IPI is

   procedure Send_IPI
     (Hart_Mask :     UWord; Hart_Mask_Base : UWord; Result : out UWord;
      Error     : out IWord);

private
   Extension_Id : constant UWord := 16#73_5049#;
end SBI.IPI;
