package body SBI.RFENCE is

   --------------------
   -- Remote_Fence_I --
   --------------------

   procedure Remote_Fence_I
     (Hart_Mask :     UWord; Hart_Mask_Base : UWord; Result : out UWord;
      Error     : out IWord)
   is
   begin
      Ecall (Extension_Id, 16#00#, Hart_Mask, Hart_Mask_Base, Error, Result);
   end Remote_Fence_I;

   -----------------------
   -- Remote_SFence_VMA --
   -----------------------

   procedure Remote_SFence_VMA
     (Hart_Mask :     UWord; Hart_Mask_Base : UWord; Start_Addr : UWord;
      Result    : out UWord; Error : out IWord)
   is
   begin
      Ecall
        (Extension_Id, 16#01#, Hart_Mask, Hart_Mask_Base, Start_Addr, Error,
         Result);
   end Remote_SFence_VMA;

   ----------------------------
   -- Remote_SFence_VMA_ASID --
   ----------------------------

   procedure Remote_SFence_VMA_ASID
     (Hart_Mask : UWord; Hart_Mask_Base : UWord; Start_Addr : UWord;
      Size      : UWord; ASID : UWord; Result : out UWord; Error : out IWord)
   is
   begin
      Ecall
        (Extension_Id, 16#02#, Hart_Mask, Hart_Mask_Base, Start_Addr, Size,
         ASID, Error, Result);
   end Remote_SFence_VMA_ASID;

   -----------------------------
   -- Remote_HFence_GVMA_VMID --
   -----------------------------

   procedure Remote_HFence_GVMA_VMID
     (Hart_Mask : UWord; Hart_Mask_Base : UWord; Start_Addr : UWord;
      Size      : UWord; VMID : UWord; Result : out UWord; Error : out IWord)
   is
   begin
      Ecall
        (Extension_Id, 16#03#, Hart_Mask, Hart_Mask_Base, Start_Addr, Size,
         VMID, Error, Result);
   end Remote_HFence_GVMA_VMID;

   ------------------------
   -- Remote_HFence_GVMA --
   ------------------------

   procedure Remote_HFence_GVMA
     (Hart_Mask : UWord; Hart_Mask_Base : UWord; Start_Addr : UWord;
      Size      : UWord; Result : out UWord; Error : out IWord)
   is
   begin
      Ecall
        (Extension_Id, 16#04#, Hart_Mask, Hart_Mask_Base, Start_Addr, Size,
         Error, Result);
   end Remote_HFence_GVMA;

   ----------------------------
   -- Remote_HFence_VMA_ASID --
   ----------------------------

   procedure Remote_HFence_VMA_ASID
     (Hart_Mask : UWord; Hart_Mask_Base : UWord; Start_Addr : UWord;
      Size      : UWord; ASID : UWord; Result : out UWord; Error : out IWord)
   is
   begin
      Ecall
        (Extension_Id, 16#05#, Hart_Mask, Hart_Mask_Base, Start_Addr, Size,
         ASID, Error, Result);
   end Remote_HFence_VMA_ASID;

   -----------------------
   -- Remote_HFence_VMA --
   -----------------------

   procedure Remote_HFence_VMA
     (Hart_Mask :     UWord; Hart_Mask_Base : UWord; Start_Addr : UWord;
      Result    : out UWord; Error : out IWord)
   is
   begin
      Ecall
        (Extension_Id, 16#06#, Hart_Mask, Hart_Mask_Base, Start_Addr, Error,
         Result);
   end Remote_HFence_VMA;

end SBI.RFENCE;
