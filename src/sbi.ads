with Interfaces;

package SBI is
   type UWord is new Interfaces.Unsigned_64;
   type IWord is new Interfaces.Integer_64;

   type UInt32 is new Interfaces.Unsigned_32;

   SBI_SUCCESS               : constant IWord := 0;
   SBI_ERR_FAILED            : constant IWord := -1;
   SBI_ERR_NOT_SUPPORTED     : constant IWord := -2;
   SBI_ERR_INVALID_PARAM     : constant IWord := -3;
   SBI_ERR_DENIED            : constant IWord := -4;
   SBI_ERR_INVALID_ADDRESS   : constant IWord := -5;
   SBI_ERR_ALREADY_AVAILABLE : constant IWord := -6;
   SBI_ERR_ALREADY_STARTED   : constant IWord := -7;
   SBI_ERR_ALREADY_STOPPED   : constant IWord := -8;

   procedure Ecall
     (Extension_Id :     UWord; Function_Id : UWord; Error : out IWord;
      Value        : out UWord);
   procedure Ecall
     (Extension_Id :     UWord; Function_Id : UWord; Arg_0 : UWord;
      Error        : out IWord; Value : out UWord);
   procedure Ecall
     (Extension_Id : UWord; Function_Id : UWord; Arg_0 : UWord; Arg_1 : UWord;
      Error        : out IWord; Value : out UWord);
   procedure Ecall
     (Extension_Id : UWord; Function_Id : UWord; Arg_0 : UWord; Arg_1 : UWord;
      Arg_2        : UWord; Error : out IWord; Value : out UWord);
   procedure Ecall
     (Extension_Id : UWord; Function_Id : UWord; Arg_0 : UWord; Arg_1 : UWord;
      Arg_2 : UWord; Arg_3 : UWord; Error : out IWord; Value : out UWord);
   procedure Ecall
     (Extension_Id : UWord; Function_Id : UWord; Arg_0 : UWord; Arg_1 : UWord;
      Arg_2        : UWord; Arg_3 : UWord; Arg_4 : UWord; Error : out IWord;
      Value        : out UWord);
end SBI;
