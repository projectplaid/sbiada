package SBI.System_Reset is

   procedure System_Reset
     (Reset_Type :     UInt32; Reset_Reason : UInt32; Result : out UWord;
      Error      : out IWord);

private
   Extension_Id : constant UWord := 16#5352_5354#;
end SBI.System_Reset;
