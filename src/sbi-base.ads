--  @summary
--  SBI "Base" extension
--
--  @description
--  The base extension is designed to be as small as possible. As such, it
--  only contains functionality for probing which SBI extensions are available
--  and for querying the version of the SBI. All functions in the base
--  extension must be supported by all SBI implementations, so there are no
--  error returns defined.
package SBI.Base is
   procedure Get_Spec_Version
     (Major : out UWord; Minor : out UWord; Error : out IWord);
   --  Returns the current SBI specification version. This function must always
   --  succeed.
   --  @param Major major number encoded in the next 7 bits
   --  @param Minor minor number of the SBI specification is encoded in the low
   --     24 bits

   type SBI_Implementations is
     (Berkeley_Boot_Loader, Open_SBI, Xvisor, KVM, Rust_SBI, Diosix, Coffer,
      Other);
   procedure Get_Implementation_Id
     (Implementation_Id   : out UWord;
      Implementation_Type : out SBI_Implementations; Error : out IWord);
   --  Returns the current SBI implementation ID, which is different for every
   --  SBI implementation.
   --  @param Implementation_Id The raw ID returned by SBI
   --  @param Implementation_Type An enumerated value of the known
   --    implementations

   procedure Get_Implementation_Version
     (Version : out UWord; Error : out IWord);
   --  Returns the current SBI implementation version. The encoding of this
   --  version number is specific to the SBI implementation.
   --  @param Version the SBI implementation version

   procedure Probe_Extension
     (Extension_Id : UWord; Result : out UWord; Error : out IWord);
   --  Determine if an SBI extension is available
   --  @param Extension_Id the extension ID to look up
   --  @param Result 0 if the given SBI extension ID (EID) is not available,
   --     or 1 if it is available unless defined as any other non-zero value
   --     by the implementation.

   procedure Get_Machine_Vendor_Id (Result : out UWord; Error : out IWord);
   --  Get the vendor ID for this board
   --  @param Result Return a value that is legal for the mvendorid CSR and 0
   --     is always a legal value for this CSR.

   procedure Get_Machine_Architecture_Id
     (Result : out UWord; Error : out IWord);
   --  Get the architecture ID for this board
   --  @param Result Return a value that is legal for the marchid CSR and 0
   --     is always a legal value for this CSR.

   procedure Get_Machine_Implementation_Id
     (Result : out UWord; Error : out IWord);
   --  Get the implementation ID for this board
   --  @param Result Return a value that is legal for the mimpid CSR and 0 is
   --     always a legal value for this CSR.

private
   Base_Extension_Id : constant UWord := 16#10#;
end SBI.Base;
