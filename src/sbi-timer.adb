package body SBI.Timer is

   ---------------
   -- Set_Timer --
   ---------------

   procedure Set_Timer (Value : UWord; Result : out UWord; Error : out IWord)
   is
   begin
      Ecall (Extension_Id, 16#00#, Value, Error, Result);
   end Set_Timer;

end SBI.Timer;
