package body SBI.IPI is

   --------------
   -- Send_IPI --
   --------------

   procedure Send_IPI
     (Hart_Mask :     UWord; Hart_Mask_Base : UWord; Result : out UWord;
      Error     : out IWord)
   is
   begin
      Ecall (Extension_Id, 16#00#, Hart_Mask, Hart_Mask_Base, Error, Result);
   end Send_IPI;

end SBI.IPI;
