with System;

package SBI.Legacy is

   procedure Set_Timer
     (Time_Value : UWord; Result : out IWord; Error : out IWord);
   procedure Console_Putchar
     (Char : Character; Result : out IWord; Error : out IWord);
   procedure Console_Getchar (Result : out IWord; Error : out IWord);
   procedure Clear_IPI (Result : out IWord; Error : out IWord);

   type Hart_Mask_Type is array (UWord range <>) of UWord with
     Pack => True, Volatile => True;

   procedure Send_IPI
     (Hart_Mask : Hart_Mask_Type; Result : out IWord; Error : out IWord);
   procedure Remote_Fence_I
     (Hart_Mask : Hart_Mask_Type; Result : out IWord; Error : out IWord);
   procedure Remote_SFence_VMA
     (Hart_Mask :     Hart_Mask_Type; Start : UWord; Size : UWord;
      Result    : out IWord; Error : out IWord);
   procedure Remote_SFence_VMA_ASID
     (Hart_Mask : Hart_Mask_Type; Start : UWord; Size : UWord; ASID : UWord;
      Result    : out IWord; Error : out IWord);
   procedure Shutdown;

private
   Timer_Extension_Id              : constant UWord := 16#00#;
   Console_Putchar_Extension_Id    : constant UWord := 16#01#;
   Console_Getchar_Extension_Id    : constant UWord := 16#02#;
   Clear_IPI_Extension_Id          : constant UWord := 16#03#;
   Send_IPI_Extension_Id           : constant UWord := 16#04#;
   Remote_Fence_Extension_Id       : constant UWord := 16#05#;
   Remote_SFence_Extension_Id      : constant UWord := 16#06#;
   Remote_SFence_ASID_Extension_Id : constant UWord := 16#07#;
   System_Shutdown_Extension_Id    : constant UWord := 16#08#;

   --  Yes, these are copied from sbi.ads, because for these legacy
   --  calls, they return signed integers

   procedure Ecall
     (Extension_Id :     UWord; Function_Id : UWord; Error : out IWord;
      Value        : out IWord);
   procedure Ecall
     (Extension_Id :     UWord; Function_Id : UWord; Arg_0 : UWord;
      Error        : out IWord; Value : out IWord);
   procedure Ecall
     (Extension_Id :     UWord; Function_Id : UWord; Arg_0 : System.Address;
      Error        : out IWord; Value : out IWord);
   procedure Ecall
     (Extension_Id : UWord; Function_Id : UWord; Arg_0 : System.Address;
      Arg_1 : UWord; Arg_2 : UWord; Error : out IWord; Value : out IWord);
   procedure Ecall
     (Extension_Id :     UWord; Function_Id : UWord; Arg_0 : System.Address;
      Arg_1        : UWord; Arg_2 : UWord; Arg_3 : UWord; Error : out IWord;
      Value        : out IWord);
end SBI.Legacy;
