# SBIAda

RISC-V Supervisor Binary Interface (SBI) library for Ada

## Build Dependencies

None, except a GNAT toolchain. This targets [Alire](https://alire.ada.dev/) as a build
manager.

## Targets

This works with RISC-V platforms only.

Note: Due to limitations in GNAT, you will not be able to build this with `alr build`
from this repository - it will complain that the platform does not support libraries.

## Usage

In your project, run the following command:

```
alr with sbiada 
```

It is also suggested you add the following to your project's `alire.toml` for now:

```
[build-profiles]
sbiada = "development"
```

This is temporary due to an issue in this library where you will get compiler errors
from the assembler when compiled with `-O3`

## License

[MIT license](https://opensource.org/license/mit/)
